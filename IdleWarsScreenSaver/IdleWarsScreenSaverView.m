//
//  IdleWarsScreenSaverView.m
//  IdleWarsScreenSaver
//
//  Created by et2e10 on 20/08/2013.
//  Copyright (c) 2013 et2e10. All rights reserved.
//

#import "IdleWarsScreenSaverView.h"
#import "socketutil.h"

@implementation IdleWarsScreenSaverView

- (id)initWithFrame:(NSRect)frame isPreview:(BOOL)isPreview
{
    if (!isPreview) {
        socket_connect();
    }
    self = [super initWithFrame:frame isPreview:isPreview];
    if (self) {
        [self setAnimationTimeInterval:1];
    }
    return self;
}

- (void)startAnimation
{
    [super startAnimation];
    //filelog("in start animation");
}

- (void)stopAnimation
{
    [super stopAnimation];
    socket_close();
    //filelog("in end animation");
}

- (void)drawRect:(NSRect)rect
{
    //filelog("draRect");
    //[[self window] setOpaque: NO];
    //[[self window] setBackgroundColor: [NSColor colorWithCalibratedWhite: 1.0 alpha: 0.0]];
    //NSWindow * myWindow = [self window];
    //myWindow.backgroundColor = [NSColor redColor];
    //[myWindow setOpaque:NO];
    //myWindow.alphaValue = 0.5f;
    
    //rect.size.width /= 4;
    //rect.size.height /= 4;
    
    //[[NSColor greenColor] set];
    //NSRectFillUsingOperation(rect, NSCompositeSourceOver);
    
    //NSString *filePath = [[NSBundle mainBundle] pathForImageResource:@"/Users/user/Downloads/IMG_4524.JPG"];
    
    //[image release];
    //NSString *meHome = [@"~/" stringByExpandingTildeInPath];
    
    //NSString * home= @"/tmp/";
    //NSString * home= NSHomeDirectory();
    NSString * file= @"/tmp/idlewars_image.png";
    //NSString * file= [[NSArray alloc] initWithObjects: home , @"idlewars_image.png"];
    
    NSImage * image =  [[NSImage alloc] initWithContentsOfFile: file];
    //NSImage * image =  [[NSImage alloc] initWithContentsOfFile: @"/Users/user/tmp/idlewars_image.png"];
    
    //NSImage * image= [NSImage imageNamed:@"/Users/user/Downloads/cat.JPG"];
    
    CGRect imageRect = CGRectMake((rect.size.width - image.size.width) / 2,
                                  (rect.size.height - image.size.height) / 2, image.size.width, image.size.height);
    CGRect fromRect = CGRectMake(0, 0, image.size.width, image.size.height);
    
    //[image drawAtPoint:NSZeroPoint fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1];
    [image drawInRect:imageRect fromRect: fromRect operation:NSCompositeSourceOver fraction:1.0];
    //NSString *widstr=[NSString stringWithFormat:@"%f",image.size.width];
    //NSString *heistr=[NSString stringWithFormat:@"%f",image.size.height];
    //filelog([widstr cString]);
    //filelog([heistr cString]);
    
    [image release];
    [file release];
    
    
}

- (void)animateOneFrame
{
    //filelog("animateOneFrame");
    //self.window.screen.visibleFrame.size.width
    //NSString *meHome = [@"~/" stringByExpandingTildeInPath];
    
    
    //NSString * home= NSHomeDirectory();
    //NSString * file= [home stringByAppendingPathComponent:@"idlewars_image.png"];
    NSString * file= @"/tmp/idlewars_image.png";
    NSImage * image =  [[NSImage alloc] initWithContentsOfFile: file];
    
    //NSImage * image =  [[NSImage alloc] initWithContentsOfFile: NSHomeDirectory()];
    //NSImage * image =  [[NSImage alloc] initWithContentsOfFile: @"/Users/user/tmp/idlewars_image.png"];
    
    //NSImage * image= [NSImage imageNamed:@"/Users/user/Downloads/cat.JPG"];
    
    CGRect imageRect = CGRectMake((self.window.screen.visibleFrame.size.width - image.size.width) / 2,
                                  (self.window.screen.visibleFrame.size.height - image.size.height) / 2, image.size.width, image.size.height);
    
    CGRect fromRect = CGRectMake(0, 0, image.size.width, image.size.height);
    
    //[image drawAtPoint:NSZeroPoint fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1];
    [image drawInRect:imageRect fromRect: fromRect operation:NSCompositeSourceOver fraction:1.0];
    
    [image release];
    [file release];
    
    return;
}

- (BOOL)hasConfigureSheet
{
    return NO;
}

- (NSWindow*)configureSheet
{
    return nil;
}

@end
