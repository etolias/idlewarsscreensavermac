//
//  socketutil.h
//  IdleWarsScreenSaver
//
//  Created by et2e10 on 20/08/2013.
//  Copyright (c) 2013 et2e10. All rights reserved.
//

#ifndef IdleWarsScreenSaver_socketutil_h
#define IdleWarsScreenSaver_socketutil_h

void socket_connect();
void socket_close();

void filelog(char * msg);

#endif
