//
//  socketutil.c
//  IdleWarsScreenSaver
//
//  Created by et2e10 on 20/08/2013.
//  Copyright (c) 2013 et2e10. All rights reserved.
//

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

int SocketFD;

void filelog(char * msg)
{
    time_t ltime;
    time(&ltime);
    char timestr[26] = {0};
    strncpy(timestr, ctime(&ltime), 25);
    
    
    FILE * f = fopen("/tmp/screensaverlog.txt", "a");
    fprintf(f, "%s > %s\n", timestr, msg);
    fclose(f);
    return;
}

void socket_connect(){
    filelog("socket_connect called\n");
    struct sockaddr_in stSockAddr;
    int Res;
    SocketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    
    if (-1 == SocketFD)
    {
        perror("cannot create socket");
        exit(EXIT_FAILURE);
    }
    
    memset(&stSockAddr, 0, sizeof(stSockAddr));
    
    stSockAddr.sin_family = AF_INET;
    stSockAddr.sin_port = htons(9876);
    Res = inet_pton(AF_INET, "127.0.0.1", &stSockAddr.sin_addr);
    
    if (0 > Res)
    {
        perror("error: first parameter is not a valid address family");
        close(SocketFD);
        //exit(EXIT_FAILURE);
    }
    else if (0 == Res)
    	{
        perror("char string (second parameter does not contain valid ipaddress)");
        close(SocketFD);
        //exit(EXIT_FAILURE);
    }
    
    if (-1 == connect(SocketFD, (struct sockaddr *)&stSockAddr, sizeof(stSockAddr)))
    {
        perror("connect failed");
        close(SocketFD);
        //exit(EXIT_FAILURE);
    }
    
    filelog("socket_connect returning\n");
    return;
}


void socket_close(){
    filelog("socket_close called\n");
    (void) shutdown(SocketFD, SHUT_RDWR);
    
    close(SocketFD);
    filelog("socket_close returning\n");
    return;
}


